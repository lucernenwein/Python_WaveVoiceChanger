# Python voice changer

In this project I wanted to test a method for lowering or highering the pitch of the voice. To do that, I'm using Python to load wav files from a folder, do the processing, and finally saving the resulting files.

As this is a trivially multi-processable task, I took advantage of Python's easy to implement multi-processing capabilities to process files in parallel.

## Pre-required discussion

Off course, modifying the pitch of a voice is a more complicated task than just stretching or contracting a signal. This would indeed create a higher or lower frequency signal, but it would also increase or decrease its length (i.e. make the person talk respectively slower or faster), which is not our goal.

Before we dig into how we can modify the pitch of a voice, it's important to understand a property of human speech. Typically, most a voice signal can be considered as "locally stable" (sounds like "aaa", "ooo", "sss", "mmm", ... but not plosives like "t" or "p"!), which means it has a constant frequency content over a few hundreds of ms. As a result, all we need to do is lower or higher the frequencies of these stable periods while keeping their lengths unchanged.

## Method for lowering voice frequency

The principle can be described in a few lines:
1. We cut the signal in a set of chunks of duration < 100ms,
2. We remove a certain duration of signal at the end of each chunk (the more we remove, the lower the final voice),
3. We stretch the chunks back to their original sizes.

As we're just removing parts of the signal, the resulting signal will have a discontinuity at each chunk extremity. This produces an annoying "cracking" noise when listening. We can reduce these discontinuities by applying a gaussian filter at the places where we "glued" the chunks together.

## Method for highering voice frequency

The algorithm is very similar to the previous one:
1. We cut the signal in a set of chunks of duration < 100ms,
2. We duplicate a certain duration of signal at the end of each chunk (the longer the duplicate duration, the higher the final voice),
3. We contract the chunks back to their original sizes.

As in previous method, we use a gaussian filter to smooth transitions between signal chunks.

## Results

![](Tests/t2_learning_computer_x.wav)*Example 1: original voice*

![](Tests/t2_learning_computer_x_chipmunk.wav)*Example 1: higher voice*

![](Tests/t2_learning_computer_x_darthvader.wav)*Example 1: lower voice*
