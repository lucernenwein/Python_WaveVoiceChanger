import scipy.io.wavfile
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mp
import copy
import os
import multiprocessing

# Project files
import processing
import file_management

m_save_plots = False
m_debug = True


def process_file(file, result_dir):
    """ Processes file. Results are saved in result_dir.
    """
    # Reading input wave file
    rate, data = scipy.io.wavfile.read(file, mmap=False)
    
    # Parameters:
    ratioLow = 1.5
    ratioHigh = 0.7
    sample_size = int(1000 * rate / 16000) # a little arbitrary value...
    sigma = 10

    # Getting a slice of data
    begin_sample = 0
    end_sample = len(data) - 1
    small_data = data[begin_sample:end_sample]
    
    # Transforming the data
    out_data_stretch = processing.stretch_soundwave(small_data, ratioLow)
    out_data_darthvader = processing.transform_soundwave(small_data, ratioLow, sample_size)
    out_data_chipmunk = processing.transform_soundwave_high(small_data, ratioHigh, sample_size)
    out_data_gaussian = processing.apply_gaussian(small_data, sigma)
    
    output_base_path = '{result_dir}\\{filename}{{}}'.format(result_dir=result_dir, filename=file_management.get_filename(file))
        
    if(m_save_plots):
        # Plotting the input and transformed signals
        plt.figure(1)

        plt.subplot(311)
        plt.plot(small_data)
        plt.ylabel('Signal')
        plt.xlabel('N of sample')
    
        plt.subplot(312)
        plt.plot(out_data_darthvader)
        plt.ylabel('Darth vader')
        plt.xlabel('N of sample')

        plt.subplot(313)
        plt.plot(out_data_chipmunk)
        plt.ylabel('Chipmunk')
        plt.xlabel('N of sample')
        
        plt.savefig(output_base_path.format('_plot.pdf'), bbox_inches='tight', dpi = 300)

        plt.clf()

    # Saving output wave files
    scipy.io.wavfile.write(output_base_path.format('_stretched.wav'), rate, out_data_stretch)
    scipy.io.wavfile.write(output_base_path.format('_darthvader.wav'), rate, out_data_darthvader)
    scipy.io.wavfile.write(output_base_path.format('_chipmunk.wav'), rate, out_data_chipmunk)
    scipy.io.wavfile.write(output_base_path.format('_gaussian.wav'), rate, out_data_gaussian)

    plt.close(1)

    print("Processed file {}".format(file))



@processing.timeit
def main():
    
    # Getting all wav files in current directory
    list_files = file_management.get_files_in_dir('.', 'wav')
    
    if not list_files:
        return

    # Creating results folder if necessary
    result_dir = '.\\results'
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)
    
    # Configuration of plot
    mp.rcParams['axes.linewidth'] = .5
    mp.rcParams['lines.linewidth'] = .1
    mp.rcParams['patch.linewidth'] = .5
    mp.rcParams['font.size'] = 6
    
    if not m_debug:
        nb_processes = 4

        # Creating pool of tasks
        pool = multiprocessing.Pool(processes=nb_processes)
    
        # Filling pool with individual tasks. 1 task = processing of 1 file
        pool.starmap(process_file, [(f, result_dir) for f in list_files], 1)

        # Closing the pool
        pool.close()
        pool.join()
    else:
        for f in list_files:
            process_file(f, result_dir)




# Main function is called only when this module was called
# (not when it was imported)
if __name__ == "__main__":
    main()
