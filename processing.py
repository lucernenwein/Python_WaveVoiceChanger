from scipy.ndimage.filters import gaussian_filter
import numpy as np
import scipy.signal
import time


def timeit(method):

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        print("{} took {} s to complete.".format(method.__name__, round(te-ts, 2)))
        
        return result
    return timed



def apply_gaussian(soundwave, sigma):
    """ Applies a gaussian filter to input soundwave.
    """
    return gaussian_filter(soundwave, sigma=sigma).astype(soundwave.dtype, copy=False)



def stretch_soundwave(soundwave, ratio):
    """ Does a simple stretching of the input soundwave.
    """
    return scipy.signal.resample(soundwave, int(len(soundwave) * ratio)).astype(soundwave.dtype, copy=False)



def transform_soundwave(soundwave, ratio, nb_samples):
    """ Separates the soundwave in chunks of nb_samples. The distortion ratio is
    applied to each chunk individually, then each chunk is cut to keep the right size.
    A gaussian filter is applied to every stitching area.
    """
    if ratio < 1:
        print('Error: the ratio cannot be less than 1')
        return

    # Initializations
    first_idx = 0
    last_idx = nb_samples - 1
    out_soundwave = np.zeros_like(soundwave)
    sigma = nb_samples / 200
    areas_indexes = []

    while last_idx <= len(soundwave):
        # Cutting current chunck
        curChunk = soundwave[first_idx:last_idx+1]
        stretchedChunk = stretch_soundwave(curChunk, ratio)
        out_soundwave[first_idx:last_idx+1] = stretchedChunk[0:nb_samples]

        # Getting next chunk indexes
        first_idx = last_idx + 1
        last_idx = first_idx + nb_samples - 1

        if last_idx <= len(soundwave):
            areas_indexes.append(last_idx)

    # Applying gaussian filter to all stitching areas
    return filter_stitching_area(out_soundwave, sigma, areas_indexes)



def transform_soundwave_high(soundwave, ratio, nb_samples):
    """ Separates the soundwave in chunks of nb_samples. The last portion of the soundwave is repeated,
    then the chunk is compressed to original size.
    A gaussian filter is applied to every stitching area.
    """
    if ratio > 1 or ratio < 0.5:
        print('Error: the ratio cannot be more than 1 or less than 0.5')
        return

    # Initializations
    first_idx = 0
    last_idx = nb_samples - 1
    out_soundwave = np.zeros_like(soundwave)
    sigma = nb_samples / 200
    areas_idx = []

    while last_idx <= len(soundwave):
        # Cutting current chunck
        curChunk = soundwave[first_idx:last_idx+1]

        # Duplicating end of the chunk
        extendedChunk = np.concatenate((curChunk, curChunk[int(-(1 / ratio - 1) * len(curChunk) - 2):-1]))

        # Compressing back to initial size
        out_soundwave[first_idx:last_idx+1] = stretch_soundwave(extendedChunk, ratio)

        # Getting next chunk indexes
        first_idx = last_idx + 1
        last_idx = first_idx + nb_samples - 1

        if last_idx <= len(soundwave):
            areas_idx.append(last_idx)
            areas_idx.append(last_idx - nb_samples * (1 - ratio))

    return filter_stitching_area(out_soundwave, sigma, areas_idx)



def filter_stitching_area(soundwave, sigma, areas_idx):
    """ Applies a gaussian filter of standard deviation sigma around all indexes in areas_idx.
    The gaussian filter is applied on a window of size [-5*sigma, 5*sigma], but values are
    kept only in [-3*sigma, 3*sigma]
    """
    # Parameters
    out_soundwave = np.copy(soundwave)
    margin = int(2*sigma)

    # Processing every stitching area
    for i in areas_idx:
        first_idx = int(i - 3*sigma)
        last_idx = int(i + 3*sigma)
        if first_idx - margin >= 0 and last_idx+1 + margin < len(soundwave) :
            out_soundwave[first_idx:last_idx+1] = apply_gaussian(soundwave[first_idx - margin:last_idx+1 + margin], sigma)[margin:-margin]

    return out_soundwave
